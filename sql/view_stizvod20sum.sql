DROP VIEW stizvod20sum;

CREATE VIEW stizvod20sum AS (
SELECT I.godina_izv, I.br_zirorn, I.br_izvoda, I.pbr_izvoda, S.prorac_indkto,
	CAST( coalesce( sum(S.st_iz_id), 0) AS DECIMAL(15,2) ) AS sum20id, 
	CAST( coalesce( sum(S.st_iz_ip), 0) AS DECIMAL(15,2) ) AS sum20ip,
 	COUNT(S.st_iz_ip)	 AS cnt
FROM izvodi AS I INNER JOIN st_izvod AS S ON (I.godina_izv = S.godina_izv AND I.br_zirorn = S.br_zirorn AND I.br_izvoda = S.br_izvoda AND I.pbr_izvoda = S.pbr_izvoda)
WHERE S.prorac_indkto = 20
GROUP BY I.godina_izv, I.br_zirorn, I.br_izvoda, I.pbr_izvoda, S.prorac_indkto)
;
