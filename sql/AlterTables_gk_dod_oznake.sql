DROP TABLE dbo.gk_dod_oznake;
CREATE TABLE dbo.gk_dod_oznake
(
   cdod_ozn      char(20)    NOT NULL,
   dod_ozn_opis  char(200)   NOT NULL
);

CREATE UNIQUE CLUSTERED INDEX ixfffcaa
   ON dbo.gk_dod_oznake (cdod_ozn ASC);
   
ALTER TABLE st_izvod ADD cdod_ozn char(20) NULL;
UPDATE st_izvod SET cdod_ozn = '';
ALTER TABLE st_izvod ALTER COLUMN cdod_ozn char(20) NOT NULL;

ALTER TABLE st_tem ADD cdod_ozn char(20) NULL;
UPDATE st_tem SET cdod_ozn = '';
ALTER TABLE st_tem ALTER COLUMN cdod_ozn char(20) NOT NULL;

ALTER TABLE kkartice ADD cdod_ozn char(20) NULL;
UPDATE kkartice SET cdod_ozn = '';
ALTER TABLE kkartice ALTER COLUMN cdod_ozn char(20) NOT NULL;
