DROP VIEW kkar_kto_sum_noPS;

CREATE VIEW kkar_kto_sum_noPS AS (
SELECT brojkonta, CAST(SUM(st_id) AS DECIMAL(15,2)) AS sumid, CAST(SUM(st_ip) AS DECIMAL(15,2)) sumip
FROM kkartice 
WHERE dat_knjiz > '2011-01-01'
GROUP BY brojkonta)
;
