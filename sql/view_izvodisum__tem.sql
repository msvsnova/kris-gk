DROP VIEW izvodisum__tem;

CREATE VIEW izvodisum__tem AS (
SELECT I.*, 
		coalesce(ST20.sum20id,0) AS sum20id, 
		coalesce(ST20.sum20ip,0) AS sum20ip,
		coalesce(ST30.sum30id,0) AS sum30id,
		coalesce(ST30.sum30ip,0) AS sum30ip,
		T.tem_id, T.tem_ip
FROM izvodi AS I INNER JOIN tem AS T ON (I.tem_corg = T.tem_corg AND I.br_tem_pr = T.br_tem)
LEFT OUTER JOIN stizvod20sum AS ST20 ON (I.godina_izv = ST20.godina_izv AND I.br_zirorn = ST20.br_zirorn AND I.br_izvoda = ST20.br_izvoda AND I.pbr_izvoda = ST20.pbr_izvoda)
LEFT OUTER JOIN stizvod30sum AS ST30 ON (I.godina_izv = ST30.godina_izv AND I.br_zirorn = ST30.br_zirorn AND I.br_izvoda = ST30.br_izvoda AND I.pbr_izvoda = ST30.pbr_izvoda))
;
