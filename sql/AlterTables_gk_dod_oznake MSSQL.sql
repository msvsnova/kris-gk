DROP TABLE dbo.gk_dod_oznake
go

CREATE TABLE dbo.gk_dod_oznake
(
   cdod_ozn      char(20)    NOT NULL,
   dod_ozn_opis  char(200)   NOT NULL
)
go


CREATE UNIQUE CLUSTERED INDEX ixfffcaa
   ON dbo.gk_dod_oznake (cdod_ozn ASC)
go


ALTER TABLE st_izvod ADD cdod_ozn char(20) NULL
go

UPDATE st_izvod SET cdod_ozn = ''
go

ALTER TABLE st_izvod ALTER COLUMN cdod_ozn char(20) NOT NULL
go


ALTER TABLE st_tem ADD cdod_ozn char(20) NULL
go

UPDATE st_tem SET cdod_ozn = ''
go

ALTER TABLE st_tem ALTER COLUMN cdod_ozn char(20) NOT NULL
go


ALTER TABLE kkartice ADD cdod_ozn char(20) NULL
go

UPDATE kkartice SET cdod_ozn = ''
go

ALTER TABLE kkartice ALTER COLUMN cdod_ozn char(20) NOT NULL
go
ALTER TABLE kskpst ADD cdod_ozn char(20)
go
UPDATE kskpst SET cdod_ozn = ''
go
ALTER TABLE kskpst ALTER COLUMN cdod_ozn char(20) NOT NULL
go