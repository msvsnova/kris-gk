DROP VIEW stizvod20sum
GO

CREATE VIEW stizvod20sum AS (
SELECT I.godina_izv, I.br_zirorn, I.br_izvoda, I.pbr_izvoda, S.prorac_indkto,
	CAST( coalesce( sum(S.st_iz_id), 0) AS DECIMAL(15,2) ) AS sum20id, 
	CAST( coalesce( sum(S.st_iz_ip), 0) AS DECIMAL(15,2) ) AS sum20ip,
 	COUNT(S.st_iz_ip)	 AS cnt
FROM izvodi AS I INNER JOIN st_izvod AS S ON (I.godina_izv = S.godina_izv AND I.br_zirorn = S.br_zirorn AND I.br_izvoda = S.br_izvoda AND I.pbr_izvoda = S.pbr_izvoda)
WHERE S.prorac_indkto = 20
GROUP BY I.godina_izv, I.br_zirorn, I.br_izvoda, I.pbr_izvoda, S.prorac_indkto)
GO

DROP VIEW stizvod30sum
GO

CREATE VIEW stizvod30sum AS (
SELECT I.godina_izv, I.br_zirorn, I.br_izvoda, I.pbr_izvoda, S.prorac_indkto,
	CAST( coalesce( sum(S.st_iz_id), 0) AS DECIMAL(15,2) ) AS sum30id, 
	CAST( coalesce( sum(S.st_iz_ip), 0) AS DECIMAL(15,2) ) AS sum30ip,
 	COUNT(S.st_iz_ip)	 AS cnt
FROM izvodi AS I INNER JOIN st_izvod AS S ON (I.godina_izv = S.godina_izv AND I.br_zirorn = S.br_zirorn AND I.br_izvoda = S.br_izvoda AND I.pbr_izvoda = S.pbr_izvoda)
WHERE S.prorac_indkto = 30
GROUP BY I.godina_izv, I.br_zirorn, I.br_izvoda, I.pbr_izvoda, S.prorac_indkto)
GO

DROP VIEW izvodisum__tem
GO

CREATE VIEW izvodisum__tem AS (
SELECT I.*, 
		coalesce(ST20.sum20id,0) AS sum20id, 
		coalesce(ST20.sum20ip,0) AS sum20ip,
		coalesce(ST30.sum30id,0) AS sum30id,
		coalesce(ST30.sum30ip,0) AS sum30ip,
		T.tem_id, T.tem_ip
FROM izvodi AS I INNER JOIN tem AS T ON (I.tem_corg = T.tem_corg AND I.br_tem_pr = T.br_tem)
LEFT OUTER JOIN stizvod20sum AS ST20 ON (I.godina_izv = ST20.godina_izv AND I.br_zirorn = ST20.br_zirorn AND I.br_izvoda = ST20.br_izvoda AND I.pbr_izvoda = ST20.pbr_izvoda)
LEFT OUTER JOIN stizvod30sum AS ST30 ON (I.godina_izv = ST30.godina_izv AND I.br_zirorn = ST30.br_zirorn AND I.br_izvoda = ST30.br_izvoda AND I.pbr_izvoda = ST30.pbr_izvoda))
GO

DROP VIEW kkar_kto_sum_noPS
GO

CREATE VIEW kkar_kto_sum_noPS AS (
SELECT brojkonta, CAST(SUM(st_id) AS DECIMAL(15,2)) AS sumid, CAST(SUM(st_ip) AS DECIMAL(15,2)) sumip
FROM kkartice 
WHERE dat_knjiz > '2011-01-01'
GROUP BY brojkonta)
GO

DROP VIEW kkar_kto_sum
GO

CREATE VIEW kkar_kto_sum AS (
SELECT brojkonta, CAST(SUM(st_id) AS DECIMAL(15,2)) AS sumid, CAST(SUM(st_ip) AS DECIMAL(15,2)) sumip
FROM kkartice 
GROUP BY brojkonta)
GO



